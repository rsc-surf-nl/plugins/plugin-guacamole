---
- name: Install Guacamole
  hosts: localhost
  gather_facts: true

  tasks:
    - name: Wait for system to become reachable
      wait_for_connection:
        timeout: 300

    - name: Gather facts for first time
      setup:

    - name: Check if the OS is Ubuntu 22.04 or higher
      set_fact:
        is_ubuntu_22_or_higher: "{{ (ansible_distribution == 'Ubuntu' and ansible_distribution_version is version('22.04', '>=')) }}"

    - name: Install dependencies
      package:
        name:
          - make
          - gcc
          - g++
          - libcairo2-dev
          - libjpeg-turbo8-dev
          - libpng-dev
          - libtool-bin
          - libossp-uuid-dev
          - libavcodec-dev
          - libavutil-dev
          - libswscale-dev
          - freerdp2-dev
          - libpango1.0-dev
          - libssh2-1-dev
          - libvncserver-dev
          - libtelnet-dev
          - libssl-dev
          - libvorbis-dev
          - libwebp-dev
        state: present

    - name: Install Tomcat9
      package:
        name:
          - tomcat9
          - tomcat9-admin
          - tomcat9-common
          - tomcat9-user
        state: present

    - name: Start and enable Tomcat9
      service:
        name: tomcat9
        state: started
        enabled: yes

    - name: Download Guacamole server
      unarchive:
        src: https://archive.apache.org/dist/guacamole/1.5.0/source/guacamole-server-1.5.0.tar.gz
        dest: /tmp/
        remote_src: yes

    - name: Execute configure script
      shell: ./configure --with-init-dir=/etc/init.d
      args:
        chdir: /tmp/guacamole-server-1.5.0

    - name: Build target
      make:
        chdir: /tmp/guacamole-server-1.5.0

    - name: Run install target
      make:
        chdir: /tmp/guacamole-server-1.5.0
        target: install

    - name: Update system's cache of installed libraries
      shell: ldconfig
      args:
        chdir: /tmp/guacamole-server-1.5.0

    - name: Create systemd override directory for xrdp
      file:
        path: /etc/systemd/system/xrdp.service.d
        state: directory

    - name: Create systemd override file for xrdp
      copy:
        content: |
          [Unit]
          After=network.target xrdp-sesman.service multi-user.target

          [Service]
          Restart=always
        dest: /etc/systemd/system/xrdp.service.d/override.conf
        mode: 0644

    - name: Reload systemd daemon
      command: systemctl daemon-reload

    - name: Start and enable guacd
      service:
        name: guacd
        state: started
        enabled: yes

    - name: Copy guacamole folder
      copy:
        src: files/guacamole/
        dest: /etc/guacamole/

    - name: Copy guacamole folder
      copy:
        src: files/noauth-config_u20.xml
        dest: /etc/guacamole/noauth-config.xml
      when: not is_ubuntu_22_or_higher

    - name: Download Guacamole client
      get_url:
        url: https://archive.apache.org/dist/guacamole/1.5.0/binary/guacamole-1.5.0.war
        dest: /etc/guacamole/guacamole.war

    - name: Create a symbolic link of the guacamole client
      file:
        src: /etc/guacamole/guacamole.war
        dest: /var/lib/tomcat9/webapps/guacamole.war
        state: link

    - name: Create extensions and lib directories
      file:
        path: /etc/guacamole/lib
        state: directory

    - name: Configure tomcat9
      lineinfile:
        path: /etc/default/tomcat9
        line: 'GUACAMOLE_HOME=/etc/guacamole'

    - name: Remove color profile pop-up
      copy:
        src: files/polkit/45-allow-colord.pkla
        dest: /etc/polkit-1/localauthority/50-local.d/45-allow-colord.pkla

    - name: Create nginx location block
      copy:
        src: files/nginx/guacamole.conf
        dest: /etc/nginx/app-location-conf.d/guacamole.conf
        mode: 0644
      when: is_ubuntu_22_or_higher

    - name: Create nginx location block
      copy:
        src: files/nginx/guacamole_u20.conf
        dest: /etc/nginx/app-location-conf.d/guacamole.conf
        mode: 0644
      when: not is_ubuntu_22_or_higher

    - name: Comment out auth-provider line in guacamole.properties
      replace:
        path: /etc/guacamole/guacamole.properties
        regexp: '^auth-provider:\s+net.sourceforge.guacamole.net.auth.noauth.NoAuthenticationProvider$'
        replace: '# auth-provider: net.sourceforge.guacamole.net.auth.noauth.NoAuthenticationProvider'
      when: is_ubuntu_22_or_higher  

    - name: add config file for pam
      copy:
        src: files/rsc_pam.conf
        dest: /etc/rsc_pam.conf
        mode: 0644
      when: is_ubuntu_22_or_higher

    - name: Check if pam_exec.so line exists
      command: grep -q '^auth\s\+\[success=2\s\+default=ignore\]\s\+pam_exec\.so\s\+log=/var/log/totp_auth\.log\s\+quiet\s\+seteuid\s\+expose_authtok\s\+/etc/rsc/totp_login\.sh' /etc/pam.d/common-auth
      register: pam_exec_check
      ignore_errors: yes
      changed_when: no
      when: is_ubuntu_22_or_higher

    - name: Add a line before the pam_exec.so line if TOTP exists
      lineinfile:
        path: /etc/pam.d/common-auth
        line: 'auth    [success=3 default=ignore]    pam_userinfo_validate.so /etc/rsc_pam.conf'
        insertbefore: '^auth\s+\[success=2\s+default=ignore\]\s+pam_exec\.so\s+log=/var/log/totp_auth\.log\s+quiet\s+seteuid\s+expose_authtok\s+/etc/rsc/totp_login\.sh'
      when: pam_exec_check.rc == 0
      when: is_ubuntu_22_or_higher and pam_exec_check.rc == 0

    - name: Add a line before the pam_exec.so line if TOTP NOT exists
      lineinfile:
        path: /etc/pam.d/common-auth
        line: 'auth    [success=2 default=ignore]    pam_userinfo_validate.so /etc/rsc_pam.conf'
        insertbefore: '^auth\s+\[success=1 default=ignore\]\s+pam_unix\.so\s+nullok\s+try_first_pass'
      when: is_ubuntu_22_or_higher and pam_exec_check.rc == 1

    - name: Add pam user validate package
      copy:
        src: files/pam_userinfo_validate_1.0.1_amd64.deb
        dest: /tmp/pam_userinfo_validate_1.0.1_amd64.deb
      when: is_ubuntu_22_or_higher

    - name: Install the pam_userinfo_validate .deb package
      apt:
        deb: /tmp/pam_userinfo_validate_1.0.1_amd64.deb
        state: present
      when: is_ubuntu_22_or_higher

    - name: Restart nginx
      service:
        name: nginx
        state: restarted

    - name: Restart tomcat9
      service:
        name: tomcat9
        state: restarted

    - name: Start guacd
      register: start_guacd
      service:
        name: guacd
        state: started

    - debug:
        msg: 'service_url: {"url": "https://{{ ansible_host }}", "tag": "web", "description": "Guacamole"}'
      when: (start_guacd is succeeded)

